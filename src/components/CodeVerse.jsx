"use client"
import { useGLTF } from "@react-three/drei";

export function CodeVerse(props) {
    const { scene } = useGLTF('/CodeVerse.glb');
    scene.addEventListener('added',()=>{
        const mesh = scene.children[0]
        const min = scene.children[0].geometry.boundingBox.min;
        const max = scene.children[0].geometry.boundingBox.max;
        mesh.position.x += min.x
        mesh.position.y += max.y
    })
    return <primitive object={scene} {...props} />
}